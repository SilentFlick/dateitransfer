import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;

public class Server {

    private boolean stop;
    private int port;
    private Logger logger;

    public Server(int port) {
        logger = Logger.getLogger(Server.class.getName());
        FileHandler fiHandler;
        try {
            String path = "logs" + File.separator;
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String logFile = path + "datatransfer_server_" +
                             format.format(Calendar.getInstance().getTime()) +
                             ".log";
            fiHandler = new FileHandler(logFile, true);
            fiHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fiHandler);
        } catch (SecurityException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        this.port = port;
    }

    public void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (!stop) {
                try {
                    // listen for a request and create a new socket for that
                    // request
                    TCP tcpSocket = new TCP(serverSocket.accept());
                    logger.info("accept " + tcpSocket.getRemoteSocketAddress());
                    // create new thread for the new socket
                    new Slave(tcpSocket, logger);
                } catch (Exception e) {
                    logger.severe(e.getMessage());
                }
            }
            System.out.println("Byebye");
        } catch (Exception e) {
            logger.severe(e.getMessage());
        }
    }

    public void stopServer() {
        this.stop = true;
        try {
            ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
            baoStream.write("CLOSE".getBytes(StandardCharsets.US_ASCII), 0, 5);
            TCP socket = new TCP("localhost", port);
            socket.send(baoStream.toByteArray());
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Slave extends Thread {
    private TCP socket;
    private Logger logger;

    public Slave(TCP socket, Logger logger) {
        this.socket = socket;
        this.logger = logger;
        this.start();
    }

    public void run() {
        try (TCP s = socket) {
            // create a directory called data, if it does not exist
            // 5 bytes "Start" ASCII string
            // 8 bytes file length
            // the rest is file name charset UTF-8
            String path = "data" + File.separator;
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            byte[] request = new byte[s.returnMTU()];
            // wait for start packet
            request = s.receive();
            if (request != null) {
                // check start packet and parse to get file name and file length
                String starString =
                    new String(Arrays.copyOfRange(request, 0, 5),
                               StandardCharsets.US_ASCII);
                if (starString.equals("CLOSE")) {
                    return;
                }
                long fileLength =
                    bytesToLong(Arrays.copyOfRange(request, 5, 13), 0, 8);
                if ((starString.equals("Start")) && (fileLength > 0)) {
                    String status = "StartPacket OK";
                    // send reply and tell the sender to send write the data
                    // packet to the stream
                    s.send(status.getBytes(StandardCharsets.US_ASCII));
                    String fileName = new String(
                        Arrays.copyOfRange(request, 13, request.length),
                        StandardCharsets.UTF_8);
                    File file = new File(dir, fileName.trim());
                    CheckedOutputStream chOutputStream =
                        new CheckedOutputStream(new FileOutputStream(file),
                                                new CRC32());
                    long pos = 0;
                    while (true) {
                        // read from the stream, write to file and maintain a
                        // checksum at the same time. So that we don't need to
                        // read the entire file to calculate the checksum.
                        request = s.receive();
                        int byteToRead = s.returnReadBytes();
                        // The method read() from OutputStream in TCP will
                        // return the number of bytes that are read.
                        // CheckedOutputStream writes those bytes to the file
                        // and calculate the checksum.
                        // We don't know the exact number of bytes that are
                        // available in the stream
                        chOutputStream.write(request, 0, byteToRead);
                        pos += byteToRead;
                        if (pos >= fileLength) {
                            break;
                        }
                    }
                    // send the checksum to the sender.
                    long chValue = chOutputStream.getChecksum().getValue();
                    s.send(longToBytes(chValue, 4));
                    chOutputStream.close();
                } else {
                    String status = "StartPacket NOT OK";
                    logger.severe(status);
                    s.send(status.getBytes(StandardCharsets.US_ASCII));
                }
            }
        } catch (Exception e) {
            logger.severe(e.getMessage());
        }
    }

    private byte[] longToBytes(long l, int size) {
        byte[] result = new byte[size];
        int len = size;
        for (int i = 0; i < len; i++) {
            result[i] = (byte)((l >> (Byte.SIZE * (--size))) & 0xFF);
        }
        return result;
    }

    private long bytesToLong(byte[] b, int offset, int size) {
        long result = 0;
        for (int i = 0; i < (size - offset); i++) {
            result <<= Byte.SIZE;
            result |= (b[offset + i] & 0xFF);
        }
        return result;
    }
}
