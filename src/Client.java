import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

public class Client {
    public Client(String address, int port, File file) {
        try {
            TCP socket = new TCP(address, port);
            sentFile(socket, address, port, file);
            socket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void sentFile(TCP socket, String address, int port, File file) {
        try {
            // Create start packet contains 5 byte "Start" ASCII + 8 byte file
            // length + 4 byte CRC32 (calculates over the entire file) + file
            // name UFT-8
            ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
            baoStream.write("Start".getBytes(StandardCharsets.US_ASCII), 0, 5);
            baoStream.write(longToBytes(file.length(), 8), 0, 8);
            byte[] fileName = file.getName().getBytes(StandardCharsets.UTF_8);
            baoStream.write(fileName, 0, fileName.length);
            socket.send(baoStream.toByteArray()); // send start packet
            baoStream.close();
            // wait the reply from server before we continue
            byte[] replyArr = socket.receive();
            if (replyArr != null) {
                String reply = new String(replyArr, StandardCharsets.US_ASCII);
                if (reply.trim().equals("StartPacket OK")) {
                    FileInputStream fis = new FileInputStream(file);
                    // maintains a checksum of the data being read.
                    // https://en.wikipedia.org/wiki/Cyclic_redundancy_check
                    // CRC32 is only reliable for detecing accidental
                    // corruption.
                    CheckedInputStream chStream =
                        new CheckedInputStream(fis, new CRC32());
                    int byteRead = socket.returnMTU();
                    long pos = 0;
                    while (true) {
                        // read up to MTU (1000) bytes from the file and write
                        // to the stream. Call method available() to get the
                        // number of remaning bytes that can be read. This
                        // allows to create an array with the exact size of
                        // bytes that are going to be read. So that we can avoid
                        // writing 0 to the stream. And we can reduce the
                        // workload on the receiver side.
                        if (chStream.available() <= byteRead) {
                            byteRead = chStream.available();
                        }
                        byte[] buffer = new byte[byteRead];
                        byteRead = chStream.read(buffer, 0, byteRead);
                        GUI.updateByteProgress(byteRead);
                        // If we write n bytes to the stream, does not mean that
                        // we can read n bytes from the stream on the other
                        // side.
                        socket.send(buffer);
                        pos += byteRead;
                        if (pos >= file.length()) {
                            break;
                        }
                    }
                    long senderChecksum = chStream.getChecksum().getValue();
                    // wait for the checksum packet from the receiver. If the
                    // receiver checksum does not match sender checksum, data is
                    // corrupted.
                    replyArr = Arrays.copyOf(socket.receive(),
                                             socket.receive().length);
                    long receiverChecksum = bytesToLong(replyArr, 0, 4);
                    // Rewind the cursor to the beginning of the file
                    GUI.updateFileProgress(1);
                    GUI.updateInputList(file.getPath());
                    System.out.println("==> Sender: " + senderChecksum);
                    System.out.println("==> Receiver: " + receiverChecksum);
                    if (senderChecksum == receiverChecksum) {
                        reply = "CRC OK: " + file.getName();
                    } else {
                        reply = "!!! CRC NOT OK:  " + file.getName();
                    }
                    // TODO: implement handler if data is corrupted
                    GUI.updateOutputArea(reply);
                    fis.close();
                    chStream.close();
                } else {
                    GUI.updateOutputArea(reply.trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] longToBytes(long l, int size) {
        byte[] result = new byte[size];
        int len = size;
        for (int i = 0; i < len; i++) {
            result[i] = (byte)((l >> (Byte.SIZE * (--size))) & 0xFF);
        }
        return result;
    }

    private long bytesToLong(byte[] b, int offset, int size) {
        long result = 0;
        for (int i = 0; i < (size - offset); i++) {
            result <<= Byte.SIZE;
            result |= (b[offset + i] & 0xFF);
        }
        return result;
    }
}
