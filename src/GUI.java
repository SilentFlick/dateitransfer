import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;

public class GUI extends JFrame {
    public static int numberOfFile;
    public static long numberOfByte;
    private static long byteSent;
    private static DefaultListModel<String> files = new DefaultListModel<>();
    private static JTextArea outJTextArea;
    private static JProgressBar filProgressBar;
    private static JProgressBar totProgressBar;
    private static JList<String> list;
    private static int percentFile;
    private static ExecutorService executor = Executors.newCachedThreadPool();
    private static Server server;
    private static LinkedBlockingQueue<File> fileQueue =
        new LinkedBlockingQueue<File>();

    public static void updateOutputArea(String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() { outJTextArea.append(msg + "\n"); }
        });
    }

    public static synchronized void updateInputList(String fileName) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                DefaultListModel<String> inputList =
                    (DefaultListModel<String>)list.getModel();
                inputList.removeElement(fileName);
                if (inputList.isEmpty()) {
                    if (!fileQueue.isEmpty()) {
                        fileQueue.clear();
                    }
                }
            }
        });
    }

    public static synchronized void updateFileProgress(final int progressFile) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                percentFile += progressFile;
                filProgressBar.setValue(percentFile);
            }
        });
    }

    public static synchronized void updateByteProgress(final int progressByte) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                byteSent += progressByte;
                long divisor = numberOfByte / 100;
                int percentByte = (int)(byteSent / divisor);
                totProgressBar.setValue(percentByte);
            }
        });
    }

    public static synchronized void resetProgressBar() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                numberOfByte = 0;
                numberOfFile = 0;
                byteSent = 0;
                percentFile = 0;
                filProgressBar.setValue(0);
                totProgressBar.setValue(0);
            }
        });
    }

    private static void addComponentsToPane(Container pane) {
        Border blackline, raisedetched, loweredetched;
        TitledBorder titledBorder;
        blackline = BorderFactory.createLineBorder(Color.black);
        raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
        loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
        // raisedbevel = BorderFactory.createRaisedBevelBorder();
        // loweredbevel = BorderFactory.createLoweredBevelBorder();
        // empty = BorderFactory.createEmptyBorder();

        String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (ClassNotFoundException e) {
            System.out.println(
                "Couldn't find class for specified look and feel: " +
                lookAndFeel);
        } catch (UnsupportedLookAndFeelException e) {
            System.out.println("Can't use the specified look and feel: " +
                               lookAndFeel);
        } catch (Exception e) {
            System.err.println("Couldn't get specified look and feel (" +
                               lookAndFeel + "), for some reason.");
            System.err.println("Using the default look and feel.");
            e.printStackTrace();
        }
        pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        pane.add(new JLabel("Data Transfer", JLabel.CENTER),
                 BorderLayout.NORTH);

        SpringLayout springLayout = new SpringLayout();
        GridLayout gridLayout = new GridLayout(0, 3);

        // Panel Left
        JPanel panelLeft = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(raisedetched, "Output");
        titledBorder.setTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 14));
        panelLeft.setBorder(titledBorder);
        panelLeft.setLayout(new BorderLayout());
        outJTextArea = new JTextArea();
        outJTextArea.setLineWrap(true);
        outJTextArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(outJTextArea);
        panelLeft.add(scrollPane, BorderLayout.CENTER);

        // Panel Right Main
        JPanel panelRight = new JPanel();
        panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.PAGE_AXIS));

        JSplitPane splitPane =
            new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panelLeft, panelRight);
        splitPane.setResizeWeight(0.5);
        splitPane.setOneTouchExpandable(true);
        splitPane.setContinuousLayout(true);
        pane.add(splitPane, BorderLayout.CENTER);

        // Panel Right Top
        JPanel pRTop = new JPanel();
        titledBorder =
            BorderFactory.createTitledBorder(loweredetched, "Receiver");
        titledBorder.setTitleJustification(TitledBorder.RIGHT);
        pRTop.setBorder(titledBorder);
        pRTop.setLayout(springLayout);
        panelRight.add(pRTop);

        // https://stackoverflow.com/a/8703807
        String[] labels = {"IP: ", "Port: "};
        int numPairs = labels.length;
        List<JTextField> fieds = new ArrayList<JTextField>();

        for (int i = 0; i < numPairs; i++) {
            JLabel label = new JLabel(labels[i], JLabel.TRAILING);
            pRTop.add(label);
            JTextField textField = new JTextField(10);
            textField.setBorder(blackline);
            // textField.setMaximumSize( textField.getPreferredSize() );
            textField.setMaximumSize(new Dimension(
                Integer.MAX_VALUE, textField.getPreferredSize().height));
            label.setLabelFor(textField);
            pRTop.add(textField);
            fieds.add(textField);
        }
        SpringUtilities.makeCompactGrid(pRTop, numPairs, 2, // rows, cols
                                        6, 6,               // initX, initY
                                        6, 6);              // xPad, yPad
        pRTop.setOpaque(true);

        // Panel Right Center
        JPanel pRCenter = new JPanel(new BorderLayout());
        titledBorder = BorderFactory.createTitledBorder(loweredetched, "Input");
        titledBorder.setTitleJustification(TitledBorder.RIGHT);
        pRCenter.setBorder(titledBorder);
        panelRight.add(pRCenter);

        JPanel jpPanel1 = new JPanel(gridLayout);
        gridLayout.setHgap(2);
        JButton chooseButton = new JButton("Choose");
        JButton sendButton = new JButton("Transfer");
        JButton receiveButton = new JButton("Receive");
        jpPanel1.add(chooseButton);
        jpPanel1.add(sendButton);
        jpPanel1.add(receiveButton);
        pRCenter.add(jpPanel1, BorderLayout.NORTH);

        JPanel jpPanel2 = new JPanel(new BorderLayout());
        list = new JList<>(files);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        JScrollPane scrollPane2 = new JScrollPane(list);
        JButton delButton = new JButton("Delete");
        jpPanel2.add(scrollPane2, BorderLayout.CENTER);
        jpPanel2.add(delButton, BorderLayout.SOUTH);
        pRCenter.add(jpPanel2, BorderLayout.CENTER);
        JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(true);
        chooseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                resetProgressBar();
                int returnVal = fc.showOpenDialog(fc);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File[] fileChoosen = fc.getSelectedFiles();
                    numberOfFile = fileChoosen.length;
                    for (int i = 0; i < fileChoosen.length; i++) {
                        fileQueue.add(fileChoosen[i]);
                        numberOfByte += (int)fileChoosen[i].length();
                        files.addElement(fileChoosen[i].getPath());
                    }
                    filProgressBar.setMaximum(numberOfFile);
                } else if (returnVal == JFileChooser.CANCEL_OPTION) {
                    return;
                }
            }
        });
        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String address = fieds.get(0).getText();
                    if (address == null) {
                        throw new IllegalArgumentException();
                    }
                    int port = Integer.parseInt(fieds.get(1).getText());
                    if (port <= 0 || port > 65535) {
                        throw new NumberFormatException();
                    }
                    while (fileQueue.size() > 0) {
                        executor.execute(new Runnable() {
                            public void run() {
                                File file = fileQueue.poll();
                                if (file != null) {
                                    new Client(address, port, file);
                                }
                            }
                        });
                    }
                } catch (NumberFormatException ne) {
                    ne.printStackTrace();
                    JOptionPane.showMessageDialog(
                        receiveButton, "You must enter a valid port number",
                        "Inane error", JOptionPane.ERROR_MESSAGE);
                } catch (IllegalArgumentException iae) {
                    iae.printStackTrace();
                    JOptionPane.showMessageDialog(
                        receiveButton, "You must enter a valid IP/Hostname",
                        "Inane error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        receiveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!receiveButton.getText().contains("Running on")) {
                    try {
                        String answer = JOptionPane.showInputDialog(
                            receiveButton, "Enter port number:");
                        if (answer == null) {
                            return;
                        }
                        int port = Integer.parseInt(answer);
                        if (port <= 0 || port > 65535) {
                            System.out.println(port);
                            throw new NumberFormatException();
                        }
                        executor.execute(new Runnable() {
                            public void run() {
                                server = new Server(port);
                                server.startServer();
                            }
                        });
                        receiveButton.setText("Running on " +
                                              Integer.toString(port));
                    } catch (NumberFormatException ne) {
                        ne.printStackTrace();
                        JOptionPane.showMessageDialog(
                            receiveButton, "You must enter a valid port number",
                            "Inane error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    server.stopServer();
                    receiveButton.setText("Receive");
                }
            }
        });
        delButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    DefaultListModel<String> files =
                        (DefaultListModel<String>)list.getModel();
                    if (list.getSelectedIndex() != -1) {
                        File removedFile = new File(list.getSelectedValue());
                        files.remove(list.getSelectedIndex());
                        fileQueue.remove(removedFile);
                        numberOfByte -= removedFile.length();
                        numberOfFile--;
                        filProgressBar.setMaximum(numberOfFile);
                        totProgressBar.setMaximum((int)numberOfByte);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        // Panel Right Bottom
        JPanel pRBot = new JPanel();
        titledBorder =
            BorderFactory.createTitledBorder(loweredetched, "Progress");
        titledBorder.setTitleJustification(TitledBorder.RIGHT);
        pRBot.setBorder(titledBorder);
        pRBot.setLayout(new BoxLayout(pRBot, BoxLayout.PAGE_AXIS));
        panelRight.add(pRBot);

        filProgressBar = new JProgressBar(0, 100);
        filProgressBar.setString("Number of Files");
        filProgressBar.setStringPainted(true);
        totProgressBar = new JProgressBar(0, 100);
        totProgressBar.setMaximum(100);
        totProgressBar.setString("Number of Bytes");
        totProgressBar.setStringPainted(true);

        pRBot.add(filProgressBar);
        pRBot.add(Box.createRigidArea(new Dimension(0, 15)));
        pRBot.add(totProgressBar);
    }

    public static void setUIFont(FontUIResource f) {
        Enumeration<Object> keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                FontUIResource orig = (FontUIResource)value;
                Font font =
                    new Font(f.getFontName(), orig.getStyle(), f.getSize());
                UIManager.put(key, new FontUIResource(font));
            }
        }
    }

    private static void initialize() {
        JFrame mainFrame = new JFrame("DATATRANSFER");
        setUIFont(new FontUIResource(new Font(Font.MONOSPACED, Font.BOLD, 14)));
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setBounds(10, 10, 600, 500);
        mainFrame.setMinimumSize(new Dimension(600, 500));
        mainFrame.setPreferredSize(new Dimension(600, 500));
        mainFrame.setResizable(true);
        addComponentsToPane(mainFrame.getContentPane());
        mainFrame.setLocationRelativeTo(null);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() { initialize(); }
        });
    }
}
