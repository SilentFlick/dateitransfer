import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class TCP implements AutoCloseable {
    private Socket socket;
    private final int MTU = 1000;
    private byte[] buffer = new byte[MTU];
    private int readBytes;
    private BufferedInputStream biStream;
    private BufferedOutputStream boStream;

    public TCP(String address, int port)
        throws UnknownHostException, IOException {
        socket = new Socket(address, port);
        initializeStreams();
    }

    public TCP(Socket socket) throws IOException {
        this.socket = socket;
        initializeStreams();
    }

    public void send(byte[] data) throws IOException {
        boStream.write(data, 0, data.length);
        boStream.flush();
    }

    public byte[] receive() throws IOException {
        readBytes = biStream.read(buffer);
        return buffer;
    }

    public SocketAddress getRemoteSocketAddress() {
        return socket.getRemoteSocketAddress();
    }

    public int returnMTU() { return this.MTU; }

    public int returnReadBytes() { return this.readBytes; }

    private void initializeStreams() throws IOException {
        biStream = new BufferedInputStream(socket.getInputStream());
        boStream = new BufferedOutputStream(socket.getOutputStream());
    }

    public void close() throws IOException {
        socket.close();
        System.out.println("Closing " + socket.toString());
    }
}
