Dateitransfer is a program to transfer files between computers over TCP. This has three main moduls:
- TCP socket
- Sender(Client) and Receiver(Server)
- GUI

The reason why I create this project is to apply my knowledge of networking, multithreading, synchronization and error detecting.

# Building and installation

- Install: OpenJDK and make(optional)
- Building:
    - `javac -d bin src/*.java`
    - `java -cp bin GUI`
or
    - `make`

# Documentation

## TCP

This is the base modul of the program, which creates new socket, writes/reads bytes to/from the stream and automatically close the socket (`implements AutoCloseable`).
Note: if sender writes n bytes to the stream does not mean receiver can read n bytes from that stream. For example: sender writes 1000 bytes and receiver receives: 500 bytes, 200 bytes and 300 bytes. 
The method `read()` from class `InputStream` returns the number of bytes that are read.

## Client and Server

Both uses `CheckedInputStream` and `CheckedOutputStream` to read/write the data(byte array) from/to a file and maintain a checksum code. The checksum I used is Cyclic redundancy check, which is suitable for data corrupting.
The server listens on a specific port and for every request he will create a new socket and a new thread to handle the task. Therefore server may be suffered from a Denial-of-service attack.

Other recommend for the server:
- static server: accept a single request and block other requests
- parallel static server: using ThreadPool to limit how many request can be handle. Other request can be added to for example `LinkedBlockingQueue`
- sequential "interleaved" server: using `Channel` with a Selector to process the orders of several clients who are active at the same time
