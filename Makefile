JC = javac
JE = java

SRC := $(wildcard src/*.java)
OUT := bin

all: build execute

build:
	$(JC) -d $(OUT) $(SRC)

execute:
	$(JE) -cp bin GUI

clean:
	$(RM) ./data/* ./logs/*

cleanall:
	$(RM) ./bin/*.class ./data/* ./logs/*
